# Groupe de desous_a

insertion_sort.php
    Le tri commence au début du tableau
    On compare les valeurs de deux indices côte à côte (il y a donc 2 pointeurs qui avancerons au fure et a mesure)
    Dans ce tri la parti a gauche des pointeurs est trié
    Si la valeur de l'indice suivant est plus petit que la valeur de l'indice précédent alors on les échange 
    on repete cette opération tant que la valeur suivante est inférieure a la valeur précédente
    Nous parcourons le tableau indice par indice en refaisant les mêmes étapes mais en incrémentant le pointeur
    On sort de la boucle quand les pointeurs sont arrivés a la fin du tableau (car tout est trié)
Utile quand le tableau est petit ou déjà trié.

selection_sort.php
    On parcourt le tableau en commencant par le premier indice, (le pointeur est au niveau du premier indice)
    on recherche la plus petite valeur du tableau puis on l'échange avec la valeur du pointeur (qui représente le premier indice au début).
    Nous parcourons le tableau indice par indice en refaisant les mêmes étapes (on déplace le pointeur vers la droite)
    la boucle s'arrete quand la valeur tout a droite est la plus grand et que tout est trié

bubble_sort.php
    compare 2 éléments côte à côte 
    si celui de gauche est plus grand on échange avec celui de droite, et on incremente directement jusqu'a la fin du tableau 
    (la valeur  maximale sera donc tout à droite)
    on refait les mêmes étapes jusqu'à que le tableau soit trié c'est a dire quand aucune valeur n'est swap lorsqu'on parcours le tableau entierement

shell_sort.php 
    Compare un élément avec un autre élément qui est a un certain écart (gap) 
     et échanger si élément de droite plus petit
    apres l'avoir échangé on vérifie si il est également plus petit que la valeur qui se situe a un gap inférieure et
     on l'échange si il est encore plus petit on repete cette action tant que la valeur est plus petite
    Refaire les memes étapes mais en reduisant l'écart (gap)

comb_sort.php
    parcours le tableau en comparrant 2 valeurs situé à un certains écart (gap) l'un de l'autre.
    Cette écart équivaut aux nombres d'éléments dans le tableau(au début). Il est divisé par 1.3 à chaque tour de boucle jusqu'a que l'écart (gap) soit inférieure à 1 (les étapes précédente sont répétés à chaque tour de boucle).

quick_sort.php
    Dans ce tri il y a un systeme de "pivot" (le pivot représente une valeur pas un indice) l'objectif est de placé correctement
    ce pivot dans le tableau ainsi les éléments a gauche de ce pivot auront une 
    valeur plus petite ou égale à ce pivot et les éléments a droite auront une
    valeur plus grande. Pour cela nous avons une variable "start" commencant à l'indice 0
    et une autre variable "end" commencant à l'indice n (nombre d'éléments dans le tableau -1) 
    le but va être de déplacer le "start" vers la droite si le pivot choisi est >= a "start" 
    de l'autre côté on déplace vers la gauche le "end" si le pivot choisi est <= a "end"
    lorsque que ces 2 conditions sont fausse cela voudra dire que la valeur de l'indice start est > au pivot
     et que la valeur de l'indice end est < au pivot nous allons donc les swap 
     pour ainsi continuer à déplacer le "start" vers la droite et le "end" vers la gauche jusqu'a que le "start" soit > a "end" quand cette condition deviendra fausse cela voudra dire que notre indice "end" représentera la place de notre pivot
    il suffit donc de échanger la valeur à l'indice "end" avec notre pivot et voila notre pivot et bien placé
    
