<?php


$b = array_shift($argv);
$a = implode(";", $argv);
$a = explode(";",$a);

function insertion_sort($a) {
    global $comparaison;
    global $it;

    for ($i = 0; ($i < count($a)); $i++) { 
        $i_min = $i;
        $comparaison++;
        $it++;
        for ($j = $i+1; ($j < count($a)); $j++) {
            $comparaison++;
            $it++;
            if ( $a[$j] < $a[$i_min]) {
                $comparaison++;
                $t             = $a[$i_min];
                $a[$i_min] = $a[$j];
                $a[$j]     = $t;
            }
        }    
    }
    return $a;
}
$timeStart = microtime(true)*1000;
echo "Série : ";
echo implode(";",$a);
echo "\n";
$a = insertion_sort($a);
echo "Résultat : ";
echo implode(";",$a);


echo ("\nNb de comparaison : " . $comparaison . "\n");
echo ("Nb d'itération : " . $it . "\n");
$timeEnd = microtime(true)*1000;
$timeEnd = $timeEnd-$timeStart;
echo "Temps (sec) : " . round($timeEnd, 2);
echo "\n";

?>