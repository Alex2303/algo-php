<?php


$b = array_shift($argv);
$a = implode(";", $argv);
$a = explode(";",$a);


function comb_sort($a) 
{
    global $comparaison;
    global $it;

    $n = count($a);
    $gap = $n;

    while ($gap >= 1) 
    {
        $comparaison++;
        for ($i = 0 ; $i < $n-$gap ; $i++)
        {
            $comparaison++;
            $it++;
            if ($a[$i] > $a[$i+$gap]) {
                $comparaison++;
                $t = $a[$i];
                $a[$i] = $a[$i+$gap];
                $a[$i+$gap] = $t;
            }
        }
        $gap /= 1.3;
    }
    return $a;
}



$timeStart = microtime(true)*1000;
echo "Série : ";
echo implode(";",$a);
echo "\n";
$a = comb_sort($a);
echo "Résultat : ";
echo implode(";",$a);


echo ("\nNb de comparaison : " . $comparaison . "\n");
echo ("Nb d'itération : " . $it . "\n");
$timeEnd = microtime(true)*1000;
$timeEnd = $timeEnd-$timeStart;
echo "Temps (sec) : " . round($timeEnd, 2);
echo "\n";

?>