<?php

$b = array_shift($argv);
$a = implode(";", $argv);
$a = explode(";",$a);

function shell_sort($a){
    global $gap;
    global $it;
    global $comparaison;

    
    $gap = floor(count($a)/2);

    while ($gap >= 1){
        $comparaison++;
        for ($i = 0; ($i+$gap) < count($a) ; $i++){
            $comparaison++;
            $it++;
            for ($z = $i ; $z >= 0 ; $z -= $gap){
                $comparaison++;
                $it++;
                if ($a[$z] > $a[$z+$gap]){
                    $comparaison++;
                    $temp = $a[$z];
                    $a[$z] = $a[$z+$gap];
                    $a[$z+$gap] = $temp;
                }
            }
        }
        $gap = floor($gap/2);
        $it;
    }
    
    return $a;
}

$timeStart = microtime(true)*1000;
echo "Série : ";
echo implode(";",$a);
echo "\n";
$a = shell_sort($a);
echo "Résultat : ";
echo implode(";",$a);
echo ("\nNb de comparaison : " . $comparaison . "\n");
echo ("Nb d'itération : " . $it . "\n");
$timeEnd = microtime(true)*1000;
$timeEnd = $timeEnd-$timeStart;
echo "Temps (sec) : " . round($timeEnd, 2);
echo "\n";

?>