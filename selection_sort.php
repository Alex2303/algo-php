<?php
 
    
    $b = array_shift($argv); 
    $a = implode(";", $argv); 
    $a = explode(";",$a); 

    function selection_sort($a)
    {
        global $comparaison;
        global $it;

        for ($i = 0; $i < count($a)-1 ; $i++) { 
            $it++;
            $comparaison++;

            $t = $i; 
            $j = $i+1; 
            while ($j < count($a)){
                $comparaison++;
                if ($a[$t] > $a[$j]) {
                    $comparaison++;
                    $t = $j; 
                }
                $j++; 
                $it++;
            }
            $temp = $a[$i];  
            $a[$i] = $a[$t];
            $a[$t] = $temp;
            
        }
        return $a;
    }

    $timeStart = microtime(true)*1000;
    echo "Série : ";
    echo implode(";",$a);
    echo "\n";
    $a = selection_sort($a);
    echo "Résultat : ";
    echo implode(";",$a);
    
    
    echo ("\nNb de comparaison : " . $comparaison . "\n");
    echo ("Nb d'itération : " . $it . "\n");
    $timeEnd = microtime(true)*1000;
    $timeEnd = $timeEnd-$timeStart;
    echo "Temps (sec) : " . round($timeEnd, 2);
    echo "\n";

    ?>