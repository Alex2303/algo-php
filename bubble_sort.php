<?php


$b = array_shift($argv);
$a = implode(";", $argv);
$a = explode(";",$a);

function bubble_sort($a)
{
    global $comparaison;
    global $it;

    for ($i=0; $i<count($a)-1; $i++){
        $comparaison++;
        $it++;
        for ($j=0; ($j<count($a)-$i-1); $j++){
            $comparaison++;
            $it++;

            if( $a[$j] > $a[$j+1]) {
                $comparaison++;
                // swap
                $t       = $a[$j];
                $a[$j]   = $a[$j+1];
                $a[$j+1] = $t;
            }
        }
    }
    return $a;
}
$timeStart = microtime(true)*1000;
echo "Série : ";
echo implode(";",$a);
echo "\n";
$a = bubble_sort($a);
echo "Résultat : ";
echo implode(";",$a);


echo ("\nNb de comparaison : " . $comparaison . "\n");
echo ("Nb d'itération : " . $it . "\n");
$timeEnd = microtime(true)*1000;
$timeEnd = $timeEnd-$timeStart;
echo "Temps (sec) : " . round($timeEnd, 2);
echo "\n";

?>